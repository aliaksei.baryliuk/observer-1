### Java Unit Test task demo repository for Autocode

* **!IMPORTANT** Try to avoid using word "test" while naming project folders in the repository that do not contain tests (except src/test used by Maven convention) as Autocode looks up for such folders when merging student and test repository to execute hidden tests during "Test" stage. For example, this project is intentionally called "java-coverage-demo" to avoid using "test" in the project name or any of the java package names
* Minimum required dependencies: JUnit 5, JaCoCo (as plugin)
* Proper pipeline type for this kind of task is Compile -> Coverage (optional: -> Quality)
* Students are expected to implement their own unit test coverage that will be measured by "Coverage" stage
* Try to provide an empty test class for students (under src/test folder) so your students can follow proper Maven project structure and avoid possible submission failures

#### Example build result for this repository in Autocode
![img.png](img.png)